#! /bin/sh
# file: tests/rbenv/rbenv.sh

testRbenvVersion()
{
  ver=0.4.0
  [ "$ver" = "`rbenv -v | awk '{print $2}'`" ]
  assertEquals "rbenv should be at version $ver" 0 $?
}

testRbenvRubyInstallVersion()
{
  ver=20160228
  export OLDPATH=$PATH
  export PATH=$HOME/.rbenv/plugins/ruby-build/bin/:$PATH
  [ "`ruby-build --version | awk '{print $2}'`" -ge $ver ]
  export PATH=$OLDPATH
  unset OLDPATH
  assertEquals "ruby-build should be at least ver $ver" 0 $?
}

testRbenvInstalledRubies()
{
  # for version in 2.1.2 2.2.2 2.3.0; do # In base images after 2016-01 this should be available
  for version in 2.0.0-p645 2.2.2 jruby-9.0.0.0; do
    (cd `mktemp -d` && rbenv local $version &>/dev/null)
    assertEquals "Couldn't activate ruby version $version" 0 $?
  done
}

. vendor/shunit2/2.1/src/shunit2

