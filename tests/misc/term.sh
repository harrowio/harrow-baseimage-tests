#! /bin/sh
# file: tests/misc/term.sh

testTERMEnvVariable()
{
  assertEquals "harrow-256color" "$TERM"
}

testHARROW_VERSIONEnvVariable()
{
  assertEquals "1.0.0" "$HARROW_VERSION"
}

testSHELLEnvVariable()
{
  assertEquals "/bin/bash" $SHELL
}

. vendor/shunit2/2.1/src/shunit2

