#! /bin/sh
# file: tests/misc/programs.sh

testGitVersionGt()
{
  ver=2.1.3
  version_gt `git --version | awk '{print $3}'` $ver
  assertEquals "Git expected to be at least version $ver" 0 $?
}

testWgetCommandFound()
{
  which wget > /dev/null 2>&1
  assertEquals "wget not found on \$PATH" 0 $?
}

testHfoldCommandFound()
{
  which hfold > /dev/null 2>&1
  assertEquals "hfold not found on \$PATH" 0 $?
}

testHeventCommandFound()
{
  which hevent > /dev/null 2>&1
  assertEquals "hevent not found on \$PATH" 0 $?
}

. vendor/version_gt
. vendor/shunit2/2.1/src/shunit2
