# Harrow Base Image Specifications

This suite details what we expect to be possible in a Harrow base image between
versions. This repository has tags that correspond to the `HARROW_VERSION` env
var which is exported to the base image.

# Running

This repository requires a GNU environment, specifically
something supporting `sort`'s `-V` option (Hint: Not
Mac OS X)

  make

# Vendored Code

This repository vendors shunit2 which is under the LGPLv2.1.
